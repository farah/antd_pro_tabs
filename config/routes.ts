export default
    [
        {
            path: '/user',
            component: '../layouts/UserLayout',
            routes: [
                {
                    name: 'login',
                    path: '/user/login',
                    component: './user/login',
                },
            ],
        },
        {
            path: '/',
            component: '../layouts/SecurityLayout',
            routes: [
                {
                    path: '/',
                    component: '../layouts/BasicLayout',
                    authority: ['admin', 'user'],
                    routes: [
                        {
                            path: '/',
                            redirect: '/welcome',
                        },
                        {
                            path: '/welcome',
                            name: 'welcome',
                            icon: 'smile',
                            component: 'Welcome',
                            level:'1',
                        },
                        {
                            path: '/admin',
                            name: 'admin',
                            icon: 'crown',
                            component: './Admin',
                            level:'1',
                            authority: ['admin'],
                            routes: [
                                {
                                    path: '/admin/sub-page',
                                    name: 'sub-page',
                                    icon: 'smile',
                                    component: './Welcome',
                                    authority: ['admin'],
                                },
                            ],
                        },
                        {
                            name: '搜索列表',
                            icon: 'smile',
                            path: '/listsearch',
                            component: './ListSearch',
                            level:'1',
                        },
                        {
                            name: 'list.table-list',
                            icon: 'table',
                            path: '/list',
                            level:'1',
                            component: './ListTableList',
                        },
                        {
                            name: '高级表单',
                            icon: 'smile',
                            path: '/formadvancedform',
                            level:'1',
                            component: './FormAdvancedForm',
                        },
                        {
                            component: './404',
                        },
                    ],
                },
                {
                    component: './404',
                },
            ],
        },
        {
            component: './404',
        },
    ]
